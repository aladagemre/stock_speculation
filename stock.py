import math
import random
import matplotlib.pyplot as plt
import networkx as nx
from utils import injectArguments
from server import Stock

class Agent:
	@injectArguments
	def __init__(self, stock, ID):
		self.neighbors = []
		self.neighbor_opinions = []
		self.trust = 0.5
		self.INFLUENCE_TRESHOLD = 0.30	
		self.NEED_FOR_MONEY = random.random()
		self.lots = random.randrange(0,5000)
		self.money = 1000.0
		self.initial_money = self.money + self.lots * self.stock.price
		self.buy_requests = []
		self.sell_requests = []
	
	def __str__(self):
		return "Agent %d" % self.ID
		
	# =========== CALCULATION / EVALUATION PART =================

	def money_on_stock(self):
		return self.lots * self.stock.price
		
	def all_money(self):
		return self.money + self.money_on_stock()
		
	def evaluate_neighbors(self):
		"""Looks at the gathered values from neighbors and imposes the average of them as his idea."""
		self.neighbor_opinions = [neighbor.trust for neighbor in self.neighbors]
		
		if not self.neighbor_opinions:
			return 0.0
			
		average = sum(self.neighbor_opinions) / len(self.neighbor_opinions)
		if abs(self.trust - average) < self.INFLUENCE_TRESHOLD:
			self.trust = average
		
		self.neighbor_opinions = []
			
	def calculate_trust_coefficient(self):
		"""
		Calculates how trust value effects the decision of buying or selling.
		"""
		# trust:0 - coeff: -1.0
		# trust:0.50 - coeff: 0.0
		# trust:1.0 - coeff: 1.0
		
		trust_coeff = -1 + self.trust * 2
		
		return trust_coeff
		
		#return random.random()
			
		
		
	def calculate_profit_coefficient(self):
		"""gives the desire to sell"""
		current_money = self.all_money()
		
		profit_rate = (current_money - self.initial_money)/self.initial_money
		#print "My profit is %.2f" % profit_rate
		
		l = ((-0.5, 1.0), (-0.3, 0.9), (0.0, -0.1), (0.1, 0.1), (0.2, 0.2), (0.3, 0.3), (0.4, 0.4), (0.5, 0.6), (0.8, 0.7), (0.9, 0.9), (1.0, 1.0))
		for pair in l:
			if profit_rate <= pair[0]:
				coeff = pair[1]
				break
		else:
			coeff = 1.0
		return coeff
			
	def calculate_trend_coefficient(self, t=100):
		"""
		Calculates the desire to buy the stock according to latest trends.
		"""
		change = self.stock.get_change_in_last(t)
		desire_to_buy = math.sin(0.05*(change+20))
		return desire_to_buy
	
	# =========== BUY AND SELL REQUESTS =================
	def sell(self, lots, fluct=None, price=None):
		fluctuation = [-0.05, 0, 0.10]
		if not fluct:
			fluct = random.choice(fluctuation)
		if not price:
			price = self.stock.price + fluct
		if price < 0.05:
			price = 0.05
		
		self.cancel_sell_requests()
		self.stock.sell(self, lots, price)
		self.sell_requests.append( (self, lots, price) )
		
	def sell_panic(self, lots):
		fluctuation = [-0.10, -0.05, 0]
		price = self.stock.price + random.choice(fluctuation)
		if price < 0.05:
			price = 0.05
		self.stock.sell(self, lots, price)
		
	def buy(self, money=None, lots=None, price=None):
		if not lots:
			lots = math.floor(money / self.stock.price)
		fluctuation = [-0.05, 0, 0.05]
		if not price:
			price = self.stock.price + random.choice(fluctuation)
		
		self.cancel_buy_requests()
		self.stock.buy(self, lots, price)
		self.buy_requests.append( (self, lots, price) )
		
	def buy_panic(self, money=None, lots=None):
		if not lots:
			lots = math.floor(money / self.stock.price)
		fluctuation = [0, 0.05, 0.10]
		self.stock.buy(self, lots, self.stock.price + random.choice(fluctuation))
	def cancel_buy_requests(self):
		for request in self.buy_requests:
			self.stock.cancel_buy( *request )
			
	def cancel_sell_requests(self):
		for request in self.sell_requests:
			self.stock.cancel_sell( *request )
			
	# =========== DECISION PART =================	
	def play(self):
		self.evaluate_neighbors()
		
		trust_coeff = self.calculate_trust_coefficient()
		#trend_coeff = self.calculate_trend_coefficient()
		sell_desire = self.calculate_profit_coefficient()
		#print "My sell desire is %.2f" % sell_desire
		#overall_desire = 0.4 * trust_coeff + 0.6 * trend_coeff - 0.2 * self.NEED_FOR_MONEY

		local_change_rate = self.stock.get_change_in_last(10)
		global_change_rate = self.stock.get_change_in_last()
		
		unit_change_rate = self.stock.get_change_in_last(2)
		
		#print "Local change rate:", local_change_rate
		#print "Unit change rate", unit_change_rate
		
		"""chance = random.random()
		if chance < 0.2:
			self.sell(lots=self.lots, price=self.stock.price*0.9)
			return
		elif chance < 0.5:
			self.buy(money=self.money, price=self.stock.price*1.5)
			return"""
		#print "%s Trust: %.2f" % (self, trust_coeff)
		if  trust_coeff > 0.3 and self.stock.sell_requests:
			#print "="*20 + "Trusting" + "=" * 20
			self.buy(money=self.money, price=self.stock.price*(1+trust_coeff/2))
			
		elif sell_desire > 0:
			self.sell(lots=self.lots*sell_desire)
			
		elif self.stock.round_num > 1 and unit_change_rate == 0.0:
			if self.buy_requests:
				request = self.buy_requests[0]
				self.buy(lots=request[1], price=request[2]+random.choice([0.05, 0.10, 0.15]))
			
			if not self.stock.sell_requests and self.stock.buy_requests:
				_max = self.stock.get_highest_buyer_price()
				
				self.cancel_buy_requests()
				self.sell(lots=self.lots, price=_max)
			if not self.stock.buy_requests and self.stock.sell_requests:
				_min = self.stock.get_lowest_seller_price()
				
				self.cancel_sell_requests()
				self.buy(money=self.money, price=_min)
				
			"""if self.sell_requests:
				request = self.sell_requests[0]
				self.sell(lots=request[1], price=request[2]-0.05)"""
				
		elif local_change_rate >= 0:
			if random.random() >= 0.5:
				self.buy(money=self.money)
			else:
				self.sell(self.lots/2)
		elif self.stock.is_floor() and self.money:
			self.buy(money=self.money/2) # TODO: add trust here
		elif self.stock.is_ceil():
			self.sell(self.lots)
		elif -0.10 < local_change_rate < 0:
			self.sell(self.lots, fluct=0.1)
		elif local_change_rate <= -0.10:
			self.sell(self.lots)
		else:
			self.sell(self.lots)
		# decide whether to buy or sell or stay
		
		# other possible features: risk taking, experience
		
		
class Speculator(Agent):
	def __init__(self, stock, ID):
		Agent.__init__(self, stock, ID)
		initial_buy = 300
		self.buy(initial_buy)
		# buy as it rises
		# sell when it rises %lambda
		
	def speculate(self):
		pass	
	
def money_table(agents):
	print "Agent\t\tStart\t\tEnd\t\tChange"
	for agent in agents:
		print "%s\t\t%.2f\t\t%.2f\t\t%.2f" % (agent, agent.initial_money, agent.money + agent.lots*agent.stock.price, 100*(agent.money + agent.lots*agent.stock.price - agent.initial_money)/agent.initial_money)
		
def create_network(agents, m):
	n = len(agents)
	G = nx.generators.random_graphs.barabasi_albert_graph(n,m)
	for i in range(n):
		G.node[i]['agent'] = agents[i]
	for i in range(n):
		agents[i].neighbors = [G.node[neighbor]['agent'] for neighbor in G.neighbors(i)]
	return G

def main():
	s = Stock(initial_price = 1.0, lots=1000)
	agents = [Agent(s, i) for i in range(30)]
	s.agents = agents
	agents[0].trust = 1.0
	agents[1].trust = 1.0
	agents[2].trust = 1.0
	agents[3].trust = 1.0
	
	g = create_network(agents,3)
	rounds = 100
	
	for i in range(rounds):
		print "============= ROUND %d ===============" % i
		for agent in agents:
			agent.play()
			
		s.play()
		print "Stocks current price:", s.price
		if s.price < -0.5:
			s.price = 0.0
		s.average_trust()
		print "====================================="
	print s.history
	money_table(agents)
	plt.plot(range(len(s.history)), s.history)
	plt.axis([0,rounds, 0.0, 2.0])
	plt.show()
	
	plt.plot(range(len(s.trust_history)), s.trust_history)
	plt.show()




main()