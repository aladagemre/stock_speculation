import math
import random
from utils import injectArguments


class Stock:
	@injectArguments
	def __init__(self, initial_price, lots):
		self.price = initial_price
		self.history = [initial_price]
		self.trust_history = []
		self.buy_requests = {}
		self.sell_requests = {}
		self.round_num = 0
		
	def is_floor(self):
		change = self.get_change_in_last(10)
		return change <= -0.10
		
	def is_ceil(self):
		change = self.get_change_in_last(10)
		return change >= 0.10
		
	def get_price_at(self, t):
		return self.history[t]

	def average_trust(self):
		l = [agent.trust for agent in self.agents]
		avg = sum(l)/len(l)
		self.trust_history.append(avg)
		return avg
		
			
	def get_change_in_last(self, t=None):
		if not t:
			return (self.price-self.initial_price)/self.initial_price
			
		try:
			prev = self.get_price_at(-t)
		except:
			prev = self.get_price_at(0)
			
		current = self.price
		change = (current-prev)/prev
		return change
	
	def buy(self, buyer, lots, price):
		#print "%s wants to buy %d lots at %.2f unit price, in total %.2f TL" % (buyer,lots,price, price*lots)
		#print "%s, %d, %.2f,%.2fTL" % (buyer,lots,price, price*lots)
		l = self.buy_requests.get(price)
		if not l:
			l = []
		l.append( (lots, buyer) )
		self.buy_requests[price] = l
		
	def sell(self, seller, lots, price):
		#print "%s wants to sell %d lots at %.2f price" % (seller,lots,price)
		#print "%s, %d, %.2f,%.2fTL" % (seller,lots,price, price*lots)
		l = self.sell_requests.get(price)
		if not l:
			l = []
		l.append( (lots, seller) )
		self.sell_requests[price] = l
		
	# seansta artis max %10 olabilir
	def cancel_buy(self, buyer, lots, price):
		requests = self.buy_requests.get(price)
	
		try:
			requests.remove( (lots, buyer) )
		except:
			pass
	
	def cancel_sell(self, seller, lots, price):
		requests = self.sell_requests.get(price)
		try:
			requests.remove( (lots, seller) )
		except:
			pass
		
	def get_highest_buyer_price(self):
		l = self.buy_requests.keys()
		if l:
			return max(l)
		else:
			return float("-inf")
		
	def get_lowest_seller_price(self):
		l = self.sell_requests.keys()
		if l:
			return min(l)
		else:
			return float("inf")
		
	def get_min_max(self):
		buy_values = sorted(self.buy_requests.keys())
		sell_values = sorted(self.sell_requests.keys())
		
		if buy_values:
			bmax = max(buy_values)
		else:
			bmax = -1.0
		
		if sell_values:
			smin = min(sell_values)
		else:
			smin = 0.0
		
		return bmax, smin
		
	def get_first_max_buyer(self):
		goon=True
		while goon:
			buy_values = sorted(self.buy_requests.keys())
			if not buy_values:
				return None
			bmax = max(buy_values)	
			max_price_buyers = self.buy_requests[bmax]
			if not max_price_buyers:
				del self.buy_requests[bmax]
			else:
				goon=False
		first_buyer = max_price_buyers.pop(0)
		self.buy_requests[bmax] = max_price_buyers
		return first_buyer
			

	def get_first_min_seller(self):
		goon = True
		while goon:
			sell_values = sorted(self.sell_requests.keys())
			if not sell_values:
				return None
			smin = min(sell_values)
			min_price_sellers = self.sell_requests[smin]
			if not min_price_sellers:
				del self.sell_requests[smin]
			else:
				goon = False
				
		first_seller = min_price_sellers.pop(0)
		self.sell_requests[smin] = min_price_sellers
		
		return first_seller
		
	def push_seller_front(self, price, lots, seller):
		"""Pushs the sell request in the front of the list of given price.
		This is useful when seller sells only portion of his lots and remaining lots
		are wanted still to be on sale."""
		
		l = self.sell_requests.get(price)
		if not l:
			l = []
		l.insert(0, (lots, seller))
		self.sell_requests[price] = l
	
	def push_buyer_front(self, price, lots, buyer):
		"""Pushs the buy request in the front of the list of given price.
		This is useful when buyer could buy only portion of his desired lots and remaining lots
		are wanted still to be on buy list."""
		
		l = self.buy_requests.get(price)
		if not l:
			l = []
		l.insert(0, (lots, buyer))
		self.buy_requests[price] = l
	
	def print_sellers_table(self):
		print "SELL TABLE"
		print "Lowest Seller:" , self.get_lowest_seller_price()
		"""for price in sorted(self.sell_requests.keys()):
			requests = self.sell_requests[price]
			for request in requests:
				print "%s\t%.2f\t%d lots" % (request[1], price, request[0])"""
	
	def print_buyers_table(self):
		print "BUY TABLE"
		print "Highest Buyer:" , self.get_highest_buyer_price()
		"""for price in sorted(self.buy_requests.keys()):
			requests = self.buy_requests[price]
			for request in requests:
				print "%s\t%.2f\t%d lots" % (request[1],price, request[0])"""
				
	def play(self):
		self.round_num += 1
		self.print_sellers_table()
		self.print_buyers_table()

		
		if not self.buy_requests:
			print "No buyers."
			return
		if not self.sell_requests:
			print "No sellers."
			return
		
		max_buyer_price, min_seller_price = self.get_min_max()	
		#print "is floor?", self.is_floor()
		#print "is ceil?", self.is_ceil()
		while max_buyer_price >= min_seller_price:# and not self.is_floor() and not self.is_ceil():
			first_buyer = self.get_first_max_buyer()
			
			
			if not first_buyer:
				#print self.buy_requests
				#self.history.append(self.price)
				return
			
				
			first_buyer_lots = int(first_buyer[0])
			if not first_buyer_lots:
				continue
			first_buyer_agent = first_buyer[1]
			
			
			#TODO Once you pop a value, you have to restore other one in case buyer or seller lots are 0.
			first_seller = self.get_first_min_seller()
			
			if not first_seller:
				#print self.sell_requests
				#self.history.append(self.price)
				return
			
			first_seller_lots = int(first_seller[0])
			if not first_seller_lots:
				continue
			first_seller_agent = first_seller[1]
			
			
			if first_buyer_lots == first_seller_lots:
				# If first buyer and first seller are same on lots, then make the transaction.
				print "Exchange occurs on %d lots of base price %.2f in total %.2f" % (first_buyer_lots, min_seller_price, first_buyer_lots*min_seller_price)
				self.price = min_seller_price
				
				first_seller_agent.lots -= first_seller_lots
				first_seller_agent.money += first_seller_lots * self.price
				
				first_buyer_agent.lots += first_buyer_lots
				first_buyer_agent.money -= first_buyer_lots * self.price
				
				
			elif first_buyer_lots < first_seller_lots:
				print "Exchange occurs on %d lots of base price %.2f in total %.2f" % (first_buyer_lots, min_seller_price, first_buyer_lots*min_seller_price)
				self.price = min_seller_price
				
				first_seller_agent.lots -= first_buyer_lots
				first_seller_agent.money += first_buyer_lots * self.price
				
				first_seller_lots -= first_buyer_lots
				self.push_seller_front(min_seller_price, first_seller_lots, first_seller_agent)
				
				first_buyer_agent.lots += first_buyer_lots
				first_buyer_agent.money -= first_buyer_lots * self.price
				
			else: # buyerlot > sellerlot
				print "Exchange occurs on %d lots of base price %.2f in total %.2f" % (first_seller_lots, min_seller_price, first_seller_lots*min_seller_price)
				self.price = min_seller_price
				
				first_seller_agent.lots -= first_seller_lots
				first_seller_agent.money += first_seller_lots * self.price
				
				first_buyer_agent.lots += first_seller_lots
				first_buyer_agent.money -= first_seller_lots * self.price
				
				first_buyer_lots -= first_seller_lots
				self.push_buyer_front(min_seller_price, first_buyer_lots, first_buyer_agent)
				
			self.history.append(self.price)
			max_buyer_price, min_seller_price = self.get_min_max()
		#self.history.append(self.price)	 
